<?php

$headers = [];
foreach (getallheaders() as $name => $value) {
    $headers[strtolower($name)] = "{$name}={$value}";
}
unset($headers['host'], $headers['accept'], $headers['connection'], $headers['user-agent']);
echo implode(' /// ', $headers);
