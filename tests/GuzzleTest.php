<?php

namespace Test;

class GuzzleTest extends ProxyTestCase
{
    public static function provider(): array
    {
        return [
            'no get parameters' => [''],
            'guzzle get parameters not array' => ['', ['guzzle' => '']],
            'guzzle empty.html' => [
                '',
                ['guzzle' => ['url' => self::buildFixturesURL('empty.html')]]
            ],
            'CURLOPT_URL no headers sent' => [
                '',
                ['guzzle' => ['url' => self::buildFixturesURL('headers.php')]]
            ],
            'CURLOPT_URL headers sent' => [
                'extra=blabla', // extra header list
                ['guzzle' => [
                    'url' => self::buildFixturesURL('headers.php'),
                    'options' => ['headers' => ['extra' => 'blabla']],
                ]]
            ],
        ];
    }
}
