<?php

namespace Test;

use GuzzleHttp\Client as Guzzle;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Panther\ProcessManager\WebServerManager;

abstract class ProxyTestCase extends TestCase
{
    private const HOST = '127.0.0.1';
    private const PORT_PROXY = 8089;
    private const PORT_FIXTURES = 8090;

    private static WebServerManager $proxyWebServerManager;
    private static WebServerManager $fixturesWebServerManager;

    abstract public static function provider(): array;

    public static function setUpBeforeClass(): void
    {
        // proxy remote web server
        self::$proxyWebServerManager = new WebServerManager(
            $documentRoot = dirname(__DIR__) . '/src',
            self::HOST,
            self::PORT_PROXY,
            "{$documentRoot}/proxy.php"
        );
        self::$proxyWebServerManager->start();

        // web server for URL testing
        self::$fixturesWebServerManager = new WebServerManager(
            $documentRoot = __DIR__ . '/fixtures',
            self::HOST,
            self::PORT_FIXTURES
        );
        self::$fixturesWebServerManager->start();
    }

    public static function tearDownAfterClass(): void
    {
        self::$proxyWebServerManager->quit();
        self::$fixturesWebServerManager->quit();
    }

    /**
     * @dataProvider provider
     */
    public function test(string $expectedContent, array $getParameters = []): void
    {
        $client = new Guzzle();

        /** @var \GuzzleHttp\Psr7\Response $response */
        $response = $client->request(
            'GET',
            sprintf(
                '%s:%s/proxy.php?%s',
                self::HOST,
                self::PORT_PROXY,
                $getParameters ? http_build_query($getParameters) : ''
            )
        );

        $content = trim($response->getBody());
        // change content by test case
        $this->handleContent($content);

        $this->assertEquals($expectedContent, $content, 'Body content');
    }

    protected function handleContent(string &$content): void
    {
        // override this method if you need to
    }

    protected static function buildFixturesURL(string $path): string
    {
        return sprintf('http://%s:%s/%s', self::HOST, self::PORT_FIXTURES, $path);
    }
}
