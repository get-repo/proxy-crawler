<?php

namespace Test;

use Symfony\Component\DomCrawler\Crawler;

class PantherTest extends ProxyTestCase
{
    public static function provider(): array
    {
        return [
            'no get parameters' => [''],
            'panther get parameters not array' => ['', ['panther' => '']],
            'panther get parameters array empty' => ['', ['panther' => []]],
            'panther javascript' => [
                'REPLACED',
                ['panther' => [
                    'url' => self::buildFixturesURL('javascript.html'),
                ]]
            ],
            'panther javascript with actions' => [
                'REPLACED AND WAITED',
                ['panther' => [
                    'url' => self::buildFixturesURL('javascript.html'),
                    'actions' => [
                        'client.waitFor("#waited", 5)',
                    ],
                ]]
            ],
        ];
    }

    /**
     * @dataProvider provider
     */
    public function test(string $expectedContent, array $getParameters = []): void
    {
        if (getEnv('CI')) {
            // test disabled on gitlab CI
            $this->markTestSkipped('Chrome issue: unknown error DevToolsActivePort file doesn\'t exist');
        }
        parent::test($expectedContent, $getParameters);
    }

    protected function handleContent(string &$content): void
    {
        if ($content) {
            // return text of page
            $content = (new Crawler($content))->filter('body')->text();
        }
    }
}
