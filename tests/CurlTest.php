<?php

namespace Test;

class CurlTest extends ProxyTestCase
{
    public static function provider(): array
    {
        return [
            'no get parameters' => [''],
            'whatever get parameters' => ['', ['whatever' => 'whatever']],
            'CURLOPT_URL empty.html' => [
                '',
                ['CURLOPT_URL' => self::buildFixturesURL('empty.html')]
            ],
            'CURLOPT_URL no headers sent' => [
                '',
                ['CURLOPT_URL' => self::buildFixturesURL('headers.php')]
            ],
            'CURLOPT_URL headers sent' => [
                'extra=blabla', // extra header list
                [
                    'CURLOPT_URL' => self::buildFixturesURL('headers.php'),
                    'CURLOPT_HTTPHEADER' => ['extra: blabla'],
                ]
            ],
        ];
    }
}
