<?php

$rootDir = dirname(__DIR__);
if (!file_exists($composerAutoloader = "{$rootDir}/vendor/autoload.php")) {
    echo 'run "composer install --no-dev --optimize-autoloader"';
    exit;
}

if ($_GET) {
    try {
        require $composerAutoloader;
        $keys = array_keys($_GET);
        if (preg_grep('/^CURLOPT_/', $keys)) {
            $ch = curl_init();
            $_GET['CURLOPT_RETURNTRANSFER'] = 1;
            $_GET['CURLOPT_HTTPHEADER'][] = 'Connection: keep-alive';
            // phpcs:ignore
            $_GET['CURLOPT_HTTPHEADER'][] = 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36';
            foreach ($_GET as $constant => $value) {
                if (preg_match('/^CURLOPT_/', $constant) && defined($constant)) {
                    curl_setopt($ch, constant($constant), $value);
                }
            }
            $output = curl_exec($ch);
            curl_close($ch);
            echo $output;
        } elseif (isset($_GET['guzzle']) && is_array($_GET['guzzle']) && $_GET['guzzle']) {
            $config = $_GET['guzzle'];
            $guzzle = new GuzzleHttp\Client();
            /** @var \GuzzleHttp\Psr7\Response $response */
            $response = $guzzle->request(
                $config['method'] ?? 'GET',
                $config['url'] ?? '',
                $config['options'] ?? [],
            );
            echo trim($response->getBody());
        } elseif (isset($_GET['panther']) && is_array($_GET['panther']) && $_GET['panther']) {
            if (!file_exists($chromeDriverPath = "{$rootDir}/drivers/chromedriver")) {
                echo 'run "vendor/bin/bdi detect drivers"';
                http_response_code(404);
                exit;
            }
            $config = $_GET['panther'];
            $findPort = function (string $host, int $killTime = 30/*seconds*/): int {
                $ports = range(9515, 9530);
                foreach ($ports as $port) {
                    $connection = @fsockopen($host, $port);
                    if (!is_resource($connection)) {
                        return $port;
                    }
                    $out = [];
                    exec("ps -o pid,etimes,command | grep '{$port}'", $out);
                    foreach ((array) $out as $cmd) {
                        if (strpos($cmd = trim($cmd), "--port={$port}")) {
                            $cmd = array_values(array_filter(explode(' ', $cmd)));
                            $pid = (int) $cmd[0];
                            $time = (int) $cmd[1];
                            if ($pid > 0 && $time > $killTime) {
                                exec("kill {$pid}");
                            }
                        }
                    }
                }

                echo 'A Panther port could not be found';
                http_response_code(404);
                exit;
            };

            $host = $config['host'] ?? '127.0.0.1';

            // display mgmt
            if (getenv('PROXY_DISPLAY') || ($config['display'] ?? false)) {
                $userAgents = json_decode((string) file_get_contents(
                    'https://raw.githubusercontent.com/microlinkhq/top-user-agents/master/src/desktop.json',
                ));
                $userAgent = $userAgents[array_rand($userAgents)];

                $replacements = [
                    '/^--headless$/' => ['replace' => '--display=:0'],
                    '/^--user-agent=/' => [
                        'keep_if_exists' => true,
                        'replace' => sprintf('--user-agent=%s', $userAgent),
                    ],
                    '/^--window-size=/' => [
                        'keep_if_exists' => true,
                        'replace' => sprintf(
                            '--window-size=%s,%s',
                            array_rand(array_flip([1024, 1080, 1280, 1440, 1536, 1600, 1620, 2048, 2100, 2400, 2880])),
                            array_rand(array_flip([2048, 2160, 2880, 4320, 8640])),
                        ),
                    ],
                    '/^--disable-gpu$/' => ['replace' => '--disable-gpu'],
                    '/^--disable-blink-features=AutomationControlled$/' => [
                        'replace' => '--disable-blink-features=AutomationControlled',
                    ],
                ];

                foreach ($replacements as $find => $replacement) {
                    $keys = preg_grep($find, $config['arguments'] ?? []);
                    if ($keys) {
                        if (!($replacement['keep_if_exists'] ?? false)) {
                            foreach (array_keys($keys) as $key) {
                                $config['arguments'][$key] = $replacement['replace'];
                            }
                        }
                    } else {
                        $config['arguments'][] = $replacement['replace'];
                    }
                }
            }

            $client = \Symfony\Component\Panther\Client::createChromeClient(
                $chromeDriverPath,
                (
                    isset($config['arguments'])
                    && (is_countable($config['arguments']) ? count($config['arguments']) : 0)
                    ?
                    $config['arguments']
                    :
                    null
                ),
                [
                    'host' => $host,
                    'port' => ($config['port'] ?? $findPort($host)),
                    'connection_timeout_in_ms' => ($config['connection_timeout_in_ms'] ?? null),
                    'request_timeout_in_ms' => ($config['request_timeout_in_ms'] ?? null),
                ]
            );

            try {
                $client->request($config['method'] ?? 'GET', $config['url'] ?? '');

                if ($actions = $config['actions'] ?? []) {
                    $el = new \Symfony\Component\ExpressionLanguage\ExpressionLanguage();
                    foreach ($actions as $action) {
                        $el->evaluate(
                            $action,
                            ['client' => $client]
                        );
                    }
                }

                // check if json
                $text = $client->getCrawler()->getText();
                if (@json_decode($text, true)) {
                    $output = $text;
                } else {
                    $output = $client->getPageSource();
                }
                $client->quit(true);
            } catch (\Exception $e) {
                $client->quit(true);
                throw $e;
            }
            echo $output;
        }
    } catch (Exception $e) {
        echo sprintf('ERROR [%d] %s', $e->getCode(), $e->getMessage());
        http_response_code(404);
        exit;
    }
}
