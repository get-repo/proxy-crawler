<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/39515079/proxy_logo.png" height=100 />
</p>

<h1 align=center>Proxy Crawler</h1>

## Table of Contents

1. [Installation](#installation)
1. [Usage](#usage)

## Installation

This is installable via [Composer](https://getcomposer.org/):

    git clone https://gitlab.com/get-repo/proxy-crawler.git proxy
    cd proxy
    composer install --no-dev --optimize-autoloader
    vendor/bin/bdi detect drivers
    # cleaning
    rm -fr .git .gitignore .gitlab-ci.yml .php-cs-fixer.php README.md composer.json composer.lock phpcs.xml phpmd.xml phpstan.neon phpunit.xml psalm.xml tests

<br/>

## Usage

### cURL
Just pass the [CURLOPT_*](https://www.php.net/manual/en/function.curl-setopt.php) options as GET parameters.
 - `CURLOPT_URL=website.fr`
 - `CURLOPT_URL=website.fr&CURLOPT_RETURNTRANSFER=1&CURLOPT_CUSTOMREQUEST=GET`
 - `CURLOPT_URL=whatismybrowser.com&CURLOPT_FOLLOWLOCATION=1&CURLOPT_HTTPHEADER[]=User-Agent%3A%20Mozilla%2F5.0`

### Guzzle
Pass all instructions in the `guzzle` GET parameter.

* **Request**
  - method: `guzzle[method]=GET`
  - url: `guzzle[url]=https://website.fr` *(protocol in URL is important!)*
* **Options**: [Guzzle Options](https://docs.guzzlephp.org/en/stable/request-options.html)
  - `guzzle[options][allow_redirects]=1`
  - `guzzle[options][headers][foo]=bar`

### Panther
Pass all instructions in the `panther` GET parameter.

* **Configure**
   - [host](https://github.com/symfony/panther?tab=readme-ov-file#changing-the-hostname-and-port-of-the-built-in-web-server): `panther[host]=127.0.0.1`
   - [port](https://github.com/symfony/panther?tab=readme-ov-file#changing-the-hostname-and-port-of-the-built-in-web-server): `panther[port]=9518` *(by default it will auto discover a port between 9515 and 9530)*
   - [chrome arguments](https://github.com/symfony/panther/blob/main/src/ProcessManager/ChromeManager.php#L95): `panther[arguments][]=--no-sandbox`
   - connection_timeout_in_ms: `panther[connection_timeout_in_ms]=10`
   - request_timeout_in_ms: `panther[request_timeout_in_ms]=25`
* **Request**
    - method: `panther[method]=GET`
    - url: `panther[url]=https://website.fr` *(protocol in URL is important!)*
* **Actions**: They are [expression language syntax](https://symfony.com/doc/current/reference/formats/expression_language.html) that calls [Panther client](https://github.com/symfony/panther/blob/main/src/Client.php) methods. Exemples:
    - `panther[actions][]=client.reload`
    - `panther[actions][]=client.waitFor(".container", 5)`
* **No headless mode**: You can open the browser with:
    - Optionally with `panther[display]=1`
    - All the time with the environment variable `PROXY_DISPLAY=1`
